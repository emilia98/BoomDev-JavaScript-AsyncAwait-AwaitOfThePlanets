import EventEmitter from "eventemitter3";
import image from "../images/planet.svg";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }
  
  constructor() {
    super();

    const box = document.createElement("div");
    box.classList.add("box");
    box.innerHTML = this._render({
      name: "Placeholder",
      terrain: "placeholder",
      population: 0,
    });

    document.body.querySelector(".main").appendChild(box);

    this._placeholder = box;
    this._loading = document.querySelector("progress");
    this._startLoading();
    this._load('https://swapi.boom.dev/api/planets');

    this.emit(Application.events.READY);
  }

  _render({ name, terrain, population }) {
    return `
<article class="media">
  <div class="media-left">
    <figure class="image is-64x64">
      <img src="${image}" alt="planet">
    </figure>
  </div>
  <div class="media-content">
    <div class="content">
    <h4>${name}</h4>
      <p>
        <span class="tag">${terrain}</span> <span class="tag">${population}</span>
        <br>
      </p>
    </div>
  </div>
</article>
    `;
  }

  async _load(url) {
    try {
      let res = await fetch(url);
      let data = await res.json();
      this._create(data);
    } catch (err) {
      console.log("Error fetching planets: ", err);
    }
  }

  _create(data) {
    let planets = data.results;
    let next = data.next;

    for (let planet of planets) {
      let article = this._render(planet);

      const box = document.createElement("div");
      box.classList.add("box");
      box.innerHTML = article;
      document.body.querySelector(".main").appendChild(box);
    }
    
    if (next) {
      this._load(next);
      return
    } 

    this._stopLoading();
  }
  
  _startLoading() {
    this._loading.style.display = "block";
  }

  _stopLoading() {
    this._loading.style.display = "none";
  }
}